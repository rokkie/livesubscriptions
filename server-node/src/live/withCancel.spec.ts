import { withCancel } from './withCancel';

describe('withCancel', () => {
  it('returns asyncIterator', async () => {
    const asyncIterator: any = { key: 'VALUE' };

    const actual = withCancel(asyncIterator, jest.fn());

    expect(actual).toEqual(asyncIterator);
  });

  it('calls onCancel when returned, and returns default result', async () => {
    const asyncIterator: any = {};

    const actual = withCancel(asyncIterator, jest.fn());
    if (!actual || !actual.return) {
      fail();
    }

    const result = await actual.return(asyncIterator);
    expect(result).toEqual({ value: undefined, done: true });
  });

  it('calls onCancel when returned, and calls return', async () => {
    const mockReturn = jest.fn();
    const asyncIterator: any = {
      return: mockReturn,
    };

    const actual = withCancel(asyncIterator, jest.fn());
    if (actual && actual.return) {
      actual.return(asyncIterator);
    }

    expect(mockReturn).toHaveBeenCalled();
  });
});
