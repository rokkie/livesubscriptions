import { LiveManager } from './liveManager';
import { _liveState } from './liveSubscribe';
import { InMemLiveState } from '../.mono/model/live-state';

jest.useFakeTimers();

describe('LiveManager', () => {
  let liveManager: LiveManager;
  let pubsubMock: { asyncIterator: jest.Mock; publish: jest.Mock };
  let nextMock: jest.Mock;

  beforeEach(() => {
    nextMock = jest.fn();
    // @ts-ignore
    _liveState = new InMemLiveState();
    pubsubMock = {
      publish: jest.fn(),
      asyncIterator: jest.fn().mockReturnValue({
        next: nextMock,
        return: jest.fn(),
      }),
    };
    liveManager = new LiveManager(pubsubMock as any, {
      topics: ['TEST_TOPIC_1', 'TEST_TOPIC_2'],
      randomGenerator: () => 'TEST_RANDOM',
    });
  });

  describe('constructor', () => {
    it('adds topics via constructor', async () => {
      expect((liveManager as any).subscriptions).toEqual({ TEST_TOPIC_1: [], TEST_TOPIC_2: [] });
    });

    it('allows for empty topics', async () => {
      const lm = new LiveManager(undefined as any);
      expect((lm as any).subscriptions).toEqual({});
    });
  });

  describe('addTopic', () => {
    it('adds topics', async () => {
      liveManager.addTopic('TEST_TOPIC_3');
      expect((liveManager as any).subscriptions).toEqual({ TEST_TOPIC_1: [], TEST_TOPIC_2: [], TEST_TOPIC_3: [] });
    });
  });

  describe('addSubscription', () => {
    it('create asyncIterator for liveId and anon user', async () => {
      liveManager.addSubscription('TEST_TOPIC_2');
      expect(pubsubMock.asyncIterator).toHaveBeenCalledWith('TEST_TOPIC_2-ANONTEST_RANDOM');
    });

    it('create asyncIterator for liveId and user', async () => {
      liveManager.addSubscription('TEST_TOPIC_2', {
        userId: 'TEST_USER_ID',
      });

      expect(pubsubMock.asyncIterator).toHaveBeenCalledWith('TEST_TOPIC_2-TEST_USER_IDTEST_RANDOM');
    });

    it('create two subscriptions two uses', async () => {
      liveManager.addSubscription('TEST_TOPIC_2');
      liveManager.addSubscription('TEST_TOPIC_2', {
        userId: 'TEST_USER_ID',
      });

      expect((liveManager as any).subscriptions).toEqual({
        TEST_TOPIC_1: [],
        TEST_TOPIC_2: ['ANONTEST_RANDOM', 'TEST_USER_IDTEST_RANDOM'],
      });
    });

    it('next returns result', async () => {
      const asyncIterator = liveManager.addSubscription('TEST_TOPIC_2');
      nextMock.mockReturnValue('TEST_RESULT');
      const result = await asyncIterator.next();

      expect(nextMock).toHaveBeenCalled();
      expect(result).toEqual('TEST_RESULT');
    });

    describe('closing async iter', () => {
      it('clears cache', async () => {
        const asyncIterator = liveManager.addSubscription('TEST_TOPIC_2');

        _liveState.set('ANONTEST_RANDOM', 'TEST_CACHE');
        nextMock.mockReturnValue('TEST_RESULT');
        if (asyncIterator && asyncIterator.return) {
          await asyncIterator.return();
        }

        expect(_liveState.get('ANONTEST_RANDOM')).toEqual(undefined);
      });

      it('removes unsubscribing live id from subscriptions', async () => {
        pubsubMock.asyncIterator.mockReturnValueOnce(
          jest.fn().mockReturnValue({
            next: nextMock,
            return: jest.fn(),
          }),
        );
        const asyncIteratorAnon = liveManager.addSubscription('TEST_TOPIC_2');
        liveManager.addSubscription('TEST_TOPIC_2', {
          userId: 'TEST_USER_ID',
        });

        expect((liveManager as any).subscriptions).toEqual({
          TEST_TOPIC_1: [],
          TEST_TOPIC_2: ['ANONTEST_RANDOM', 'TEST_USER_IDTEST_RANDOM'],
        });

        nextMock.mockReturnValue('TEST_RESULT');
        if (asyncIteratorAnon && asyncIteratorAnon.return) {
          await asyncIteratorAnon.return();
        }

        expect((liveManager as any).subscriptions).toEqual({
          TEST_TOPIC_1: [],
          TEST_TOPIC_2: ['TEST_USER_IDTEST_RANDOM'],
        });

        nextMock.mockReturnValue('TEST_RESULT');
        if (asyncIteratorAnon && asyncIteratorAnon.return) {
          await asyncIteratorAnon.return();
        }

        expect((liveManager as any).subscriptions).toEqual({
          TEST_TOPIC_1: [],
          TEST_TOPIC_2: ['TEST_USER_IDTEST_RANDOM'],
        });
      });
    });
  });

  describe('publish', () => {
    beforeEach(() => {
      liveManager.addSubscription('TEST_TOPIC_2');
    });

    it('publishes to all liveIds on a topic', async () => {
      await liveManager.publish('TEST_TOPIC_2');

      expect(pubsubMock.publish).toHaveBeenCalledWith('TEST_TOPIC_2-ANONTEST_RANDOM', {
        TEST_TOPIC_2: { liveId: 'ANONTEST_RANDOM' },
      });
    });

    it('publishes to all liveIds on a topic, with content', async () => {
      await liveManager.publish('TEST_TOPIC_2', { key: 'TEST_VALUE' });

      expect(pubsubMock.publish).toHaveBeenCalledWith('TEST_TOPIC_2-ANONTEST_RANDOM', {
        TEST_TOPIC_2: { liveId: 'ANONTEST_RANDOM', key: 'TEST_VALUE' },
      });
    });
  });
});
