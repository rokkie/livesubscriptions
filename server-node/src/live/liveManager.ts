import { LiveData } from '../.mono/model/model';
import { withCancel } from './withCancel';
import { _liveState } from './liveSubscribe';
import { withWrapper } from './withWrapper';

export interface LivePubSub {
  publish(triggerName: string, payload: any): Promise<void>;

  subscribe(triggerName: string, onMessage: Function, options: Object): Promise<number>;

  asyncIterator<T>(triggers: string | string[]): AsyncIterator<T>;
}

export interface LiveManagerOptions {
  topics?: string[];
  randomGenerator?: () => string;
}

export class LiveManager {
  private subscriptions: Record<string, string[]> = {};
  private randomGenerator: () => string;

  constructor(private pubSub: LivePubSub, options: LiveManagerOptions = {}) {
    options.topics?.forEach(topic => this.addTopic(topic));
    this.randomGenerator = options.randomGenerator ?? (() => `${Math.random()}`.substr(2));
  }

  addTopic(topic: string) {
    this.subscriptions[topic] = [];
  }

  async publish(topic: string, content: unknown = null): Promise<void> {
    const liveIds = this.subscriptions[topic];
    for (let i = 0; i < liveIds?.length; i++) {
      await this.publishForLiveId(topic, liveIds[i], content);
    }
  }

  async publishForLiveId(topic: string, liveId: string, content: any = null): Promise<void> {
    const liveData: LiveData = { ...(content ?? {}), liveId };
    return this.pubSub.publish(this.buildTriggerName(topic, liveId), { [topic]: liveData });
  }

  addSubscription<T extends LiveData>(
    topic: string,
    { userId, content, preTrigger, postTrigger }: AddSubscriptionProps<T> = {},
  ): AsyncIterator<T> {
    const liveId = `${userId ?? 'ANON'}${this.randomGenerator()}`;
    this.subscriptions[topic].push(liveId);

    let postTriggerFired = false;
    const rePublish = setInterval(async () => {
      postTriggerFired ? clearInterval(rePublish) : await this.publishForLiveId(topic, liveId, content);
    }, 10);

    setTimeout(() => this.publishForLiveId(topic, liveId, content), 1);
    return withWrapper(
      withCancel(this.pubSub.asyncIterator<T>(this.buildTriggerName(topic, liveId)), () => {
        const liveIdIndex = this.subscriptions[topic].indexOf(liveId);
        if (liveIdIndex >= 0) {
          this.subscriptions[topic].splice(liveIdIndex, 1);
        }
        if (this.subscriptions[topic].indexOf(liveId) < 0) {
          _liveState?.delete(liveId);
        }
      }),
      preTrigger,
      value => {
        postTriggerFired = true;
        postTrigger && postTrigger(value);
      },
    );
  }

  private buildTriggerName(topic: string, liveId: string): string {
    return `${topic}-${liveId}`;
  }
}

export interface AddSubscriptionProps<T> {
  userId?: string | null;
  content?: unknown | null;
  preTrigger?: () => void | unknown;
  postTrigger?: (result: IteratorResult<T>) => void | unknown;
}
