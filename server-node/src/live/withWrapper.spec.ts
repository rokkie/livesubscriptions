import { withWrapper } from './withWrapper';

describe('withWrapper', () => {
  it('returns asyncIterator', async () => {
    const nextMock = jest.fn();
    const asyncIterator: any = { next: nextMock };

    const actual = withWrapper(asyncIterator);

    expect(actual).toEqual(asyncIterator);
  });

  it('call next when next was called, and returns result', async () => {
    const nextMock = jest.fn();
    nextMock.mockResolvedValue('TEST_RESOLVED_VALUE');
    const asyncIterator: any = { next: nextMock };

    const result = await withWrapper(asyncIterator).next();

    expect(nextMock).toHaveBeenCalled();
    expect(result).toEqual('TEST_RESOLVED_VALUE');
  });

  it('calls pre and post', async () => {
    const nextMock = jest.fn();
    nextMock.mockResolvedValue('TEST_RESOLVED_VALUE');
    const asyncIterator: any = { next: nextMock };

    const prePostMock = jest.fn();
    await withWrapper(asyncIterator, prePostMock, prePostMock).next();

    expect(prePostMock).toHaveBeenNthCalledWith(1);
    expect(prePostMock).toHaveBeenNthCalledWith(2, 'TEST_RESOLVED_VALUE');
  });
});
