import {
  _idFieldsByTypename,
  _liveKeyword,
  _liveState,
  _objectIdentifier,
  liveSubscribeBuilder,
  LiveSubscribeBuilderConfig,
} from './liveSubscribe';
import { InMemLiveState } from '../.mono/model/live-state';
import { SubscribeFunction } from 'subscriptions-transport-ws/dist/server';

describe('liveSubscribe', () => {
  describe('builder', () => {
    it('store config params and creates liveSubscribe', async () => {
      const config = {
        liveState: new InMemLiveState(),
        liveKeyword: 'TEST_LIVE',
        idFieldsByTypename: { contents: 'id' },
        objectIdentifier: jest.fn(),
      };

      const liveSubscribe = liveSubscribeBuilder(jest.fn(), config);

      expect(liveSubscribe).toBeDefined();
      expect(_liveState).toEqual(config.liveState);
      expect(_liveKeyword).toEqual(config.liveKeyword);
      expect(_idFieldsByTypename).toEqual(config.idFieldsByTypename);
      expect(_objectIdentifier).toEqual(config.objectIdentifier);
    });

    it('has sensible defaults', async () => {
      const liveSubscribe = liveSubscribeBuilder(jest.fn());

      expect(liveSubscribe).toBeDefined();
      expect(_liveState).toBeDefined();
      expect(_liveKeyword).toEqual('live');
      expect(_idFieldsByTypename).toEqual({});
      expect(_objectIdentifier).toBeUndefined();
    });
  });

  describe('liveSubscribe', () => {
    let config: LiveSubscribeBuilderConfig;
    let liveSubscribe: SubscribeFunction;
    let nextMock: jest.Mock;

    const subscribeMock = jest.fn();

    beforeEach(() => {
      config = {
        liveState: new InMemLiveState(),
        liveKeyword: 'TEST_LIVE',
        idFieldsByTypename: { contents: 'id' },
        objectIdentifier: jest.fn(),
      };

      nextMock = jest.fn();
      subscribeMock.mockReturnValue({ next: nextMock });

      liveSubscribe = liveSubscribeBuilder(subscribeMock, config);
    });

    it('passes params to subscribe', async () => {
      const result = liveSubscribe(
        'TEST_SCHEMA' as any,
        'TEST_DOCUMENT' as any,
        'TEST_ROOT_VALUE' as any,
        'TEST_CONTEXT_VALUE' as any,
        'TEST_VARIABLE_VALUES' as any,
        'TEST_OPERATION_NAME' as any,
        'TEST_FIELD_RESOLVER' as any,
        'TEST_SUBSCRIBE_FIELD_RESOLVER' as any,
      );

      expect(subscribeMock).toHaveBeenCalledWith(
        'TEST_SCHEMA' as any,
        'TEST_DOCUMENT' as any,
        'TEST_ROOT_VALUE' as any,
        'TEST_CONTEXT_VALUE' as any,
        'TEST_VARIABLE_VALUES' as any,
        'TEST_OPERATION_NAME' as any,
        'TEST_FIELD_RESOLVER' as any,
        'TEST_SUBSCRIBE_FIELD_RESOLVER' as any,
      );

      expect(result).toBeDefined();
    });

    it('does not interact with non live data', async () => {
      const testData = { value: { data: { nonLive: 'TEST_CONTENT' } } };
      const testDataCopy = JSON.parse(JSON.stringify(testData));
      nextMock.mockResolvedValue(testData);
      const actual = await (await (liveSubscribe as any)()).next();

      expect(actual).toEqual(testDataCopy);
    });

    describe('for live data', () => {
      it('saves data in state the first time, and returns unmodified data', async () => {
        const testData = {
          value: {
            data: {
              [config.liveKeyword + '_A']: { liveId: 'LIVE_ID_A', content: 'TEST_CONTENT_A' },
              [config.liveKeyword + '_B']: { liveId: 'LIVE_ID_B', content: 'TEST_CONTENT_B' },
            },
          },
        };
        const testDataCopy = JSON.parse(JSON.stringify(testData));
        nextMock.mockResolvedValue(testData);

        const actual = await (await (liveSubscribe as any)()).next();

        expect(_liveState.get('LIVE_ID_A')).toEqual(testDataCopy.value.data[config.liveKeyword + '_A']);
        expect(_liveState.get('LIVE_ID_B')).toEqual(testDataCopy.value.data[config.liveKeyword + '_B']);
        expect(actual).toEqual(testDataCopy);
      });

      it('second time modifies data', async () => {
        const testData = {
          value: {
            data: {
              [config.liveKeyword + '_A']: { liveId: 'LIVE_ID_A', content: 'TEST_CONTENT_A' },
              [config.liveKeyword + '_B']: { liveId: 'LIVE_ID_B', content: 'TEST_CONTENT_B' },
              [config.liveKeyword + '_C']: {
                liveId: 'LIVE_ID_C',
                contents: [
                  { id: 'C1', data: 'TEST_CONTENT_C1' },
                  { id: 'C2A', data: 'TEST_CONTENT_C2A' },
                  { id: 'C2B', data: 'TEST_CONTENT_C2B' },
                  { id: 'C3', data: 'TEST_CONTENT_C3' },
                ],
              },
            },
          },
        };
        const testDataCopy = JSON.parse(JSON.stringify(testData));
        _liveState.set('LIVE_ID_A', { ...testData.value.data[config.liveKeyword + '_A'], oldKey: 'OLD_VALUE_1' });
        _liveState.set('LIVE_ID_B', testData.value.data[config.liveKeyword + '_B']);
        _liveState.set('LIVE_ID_C', {
          ...testData.value.data[config.liveKeyword + '_C'],
          contents: [
            { id: 'C2A', data: 'TEST_CONTENT_C2A', __typename: 'contents' },
            { id: 'C3', data: 'TEST_CONTENT_C3', __typename: 'contents' },
            { id: 'C1', data: 'TEST_CONTENT_C1_OLD', __typename: 'contents' },
            { id: 'C2B', data: 'TEST_CONTENT_C2B', __typename: 'contents' },
          ],
        });

        nextMock.mockResolvedValue(testData);

        const actual = await (await (liveSubscribe as any)()).next();

        expect(actual.value.data[config.liveKeyword + '_A']).toEqual({
          liveId: 'LIVE_ID_A',
          count: 1,
          diff: '{"oldKey":["OLD_VALUE_1",0,0]}',
        });
        expect(actual.value.data[config.liveKeyword + '_B']).toEqual({
          liveId: 'LIVE_ID_B',
          count: 1,
          patch: '[]',
        });
        expect(actual.value.data[config.liveKeyword + '_C']).toEqual({
          liveId: 'LIVE_ID_C',
          count: 1,
          diff: '{"contents":{"0":[{"id":"C1","data":"TEST_CONTENT_C1"}],"1":[{"id":"C2A","data":"TEST_CONTENT_C2A"}],"2":[{"id":"C2B","data":"TEST_CONTENT_C2B"}],"3":[{"id":"C3","data":"TEST_CONTENT_C3"}],"_t":"a","_0":[{"id":"C2A","data":"TEST_CONTENT_C2A","__typename":"contents"},0,0],"_1":[{"id":"C3","data":"TEST_CONTENT_C3","__typename":"contents"},0,0],"_2":[{"id":"C1","data":"TEST_CONTENT_C1_OLD","__typename":"contents"},0,0],"_3":[{"id":"C2B","data":"TEST_CONTENT_C2B","__typename":"contents"},0,0]}}',
        });
      });
    });
  });
});
