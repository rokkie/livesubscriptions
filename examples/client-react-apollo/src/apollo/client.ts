import { ApolloClient, ApolloLink, HttpLink, InMemoryCache, split } from '@apollo/client';
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';
import { liveRequestHandlerBuilder } from '../.mono/client-apollo/apollo/live/liveRequestHandler';

const httpLink = new HttpLink({
  uri: 'http://localhost:9900/graphql',
});

const wsLink = new WebSocketLink({
  uri: 'ws://localhost:9900/subscriptions',
  options: {
    reconnect: true,
  },
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
  },
  wsLink,
  httpLink,
);

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: ApolloLink.from([liveRequestHandlerBuilder(), splitLink]),
});
