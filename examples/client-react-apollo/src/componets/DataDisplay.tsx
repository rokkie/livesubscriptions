import { Tree } from 'antd';
import { DataNode } from 'rc-tree/lib/interface';

export interface DataDisplayProps {
  liveExampleListData: {
    someNumber: string;
    array: string[];
    nestedArray: [
      {
        someText: string;
        anotherArray: string[];
      },
    ];
  }[];
}

export const DataDisplay: React.FC<DataDisplayProps> = ({ liveExampleListData }) => {
  if (!liveExampleListData || liveExampleListData.length <= 0) {
    return null;
  }

  const treeData: DataNode[] = liveExampleListData.map((data, index) => {
    return {
      key: `root-${index}`,
      title: 'Entry',
      children: [
        {
          key: `root-${index}-someNumber`,
          title: `someNumber: ${data.someNumber ?? 'missing someNumber'}`,
        },
        {
          key: `root-${index}-array`,
          title: `array: ${data.array?.toString() ?? 'missing array'}`,
        },
        {
          key: `root-${index}-nestedData`,
          title: 'nestedArray',
          children: data.nestedArray.map((na, naIndex) => ({
            key: `root-${index}-nestedArray-${naIndex}`,
            title: `nestedArray-${naIndex}`,
            children: [
              {
                key: `root-${index}-nestedArray-${naIndex}-someText`,
                title: `someText: ${na.someText ?? 'missing someText'}`,
              },
              {
                key: `root-${index}-nestedArray-${naIndex}¬-anotherArray`,
                title: `anotherArray: ${JSON.stringify(na.anotherArray) ?? 'missing someText'}`,
              },
            ],
          })),
        },
      ],
    };
  });

  return <Tree treeData={treeData} defaultExpandAll />;
};
