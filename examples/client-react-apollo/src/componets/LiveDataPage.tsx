import React from 'react';
import { DataDisplay } from './DataDisplay';
import { gql, useSubscription } from '@apollo/client';
import { Divider } from 'antd';
import { _liveState } from '../.mono/client-apollo/apollo/live/liveRequestHandler';

export const LiveDataPage: React.FC = () => {
  const { data, error } = useSubscription(gql`
    subscription OnLiveExampleSubscription {
      liveExampleListData {
        liveId
        exampleData {
          someNumber
          array
          nestedArray {
            someText
            anotherArray {
              keyA
              keyB
            }
          }
        }
        listData {
          someNumber
          array
          nestedArray {
            someText
            anotherArray {
              keyA
              keyB
            }
          }
        }
      }
    }
  `);

  if (error) {
    console.log(`my error`, error);
  }
  return (
    <div>
      <p>{data?.liveExampleListData?.liveId}</p>
      {error ? <p>{error.toString()}</p> : <DataDisplay liveExampleListData={data?.liveExampleListData?.listData} />}
      <Divider />
      <div>{JSON.stringify(_liveState)}</div>
    </div>
  );
};
