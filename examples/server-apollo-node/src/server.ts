import express from 'express';
import { createServer } from 'http';
import compression from 'compression';
import cors from 'cors';
import { execute, subscribe } from 'graphql';
import { schema } from './schema';
import { graphqlHTTP } from 'express-graphql';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { liveSubscribeBuilder } from '../src/.mono/server-node/live/liveSubscribe';

const main = async () => {
  const PORT = process.env.PORT || 9900;
  const app = express();
  // @ts-ignore
  app.use('*', cors());
  // app.use(helmet());
  app.use(compression());
  const loggingMiddleware = (req: { ip: any }, res: any, next: () => void) => {
    console.log('ip:', req.ip);
    next();
  };
  app.use(loggingMiddleware);

  app.use(
    '/graphql',
    graphqlHTTP({
      schema,
      graphiql: true,
    }),
  );

  const httpServer = createServer(app);
  httpServer.listen(PORT, (): void => {
    console.log(`🚀GraphQL-Server is running on http://localhost:${PORT}/graphql`);
    new SubscriptionServer(
      {
        execute,
        subscribe: liveSubscribeBuilder(subscribe, {
          idFieldsByTypename: {
            ExampleData: 'someNumber',
            NestedData: 'someText',
            Keyed: 'keyA',
          },
        }),
        schema,
      },
      {
        server: httpServer,
        path: '/subscriptions',
      },
    );
  });
};

main()
  .then(() => console.log(`running...`))
  .catch(e => console.error(`FATAL ERROR`, e));
