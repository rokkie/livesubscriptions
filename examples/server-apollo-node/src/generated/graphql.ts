import { GraphQLResolveInfo } from 'graphql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } &
  { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type GqlExampleData = {
  __typename?: 'ExampleData';
  someNumber?: Maybe<Scalars['String']>;
  array?: Maybe<Array<Scalars['String']>>;
  nestedData?: Maybe<GqlNestedData>;
};

export type GqlExampleListData = {
  __typename?: 'ExampleListData';
  liveId: Scalars['String'];
  exampleData?: Maybe<GqlExampleData>;
  listData?: Maybe<Array<Maybe<GqlExampleData>>>;
};

export type GqlNestedData = {
  __typename?: 'NestedData';
  someText?: Maybe<Scalars['String']>;
  anotherArray?: Maybe<Array<Scalars['String']>>;
};

export type GqlQuery = {
  __typename?: 'Query';
  getAllUsers?: Maybe<Array<Maybe<GqlUser>>>;
};

export type GqlSubscription = {
  __typename?: 'Subscription';
  liveExampleListData?: Maybe<GqlExampleListData>;
};

export type GqlSubscriptionLiveExampleListDataArgs = {
  liveId: Scalars['String'];
};

export type GqlUser = {
  __typename?: 'User';
  name?: Maybe<Scalars['String']>;
};

export type ResolverTypeWrapper<T> = Promise<T> | T;

export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> =
  | LegacyStitchingResolver<TResult, TParent, TContext, TArgs>
  | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | ResolverWithResolve<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo,
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo,
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo,
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo,
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (
  obj: T,
  context: TContext,
  info: GraphQLResolveInfo,
) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo,
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type GqlResolversTypes = {
  ExampleData: ResolverTypeWrapper<GqlExampleData>;
  String: ResolverTypeWrapper<Scalars['String']>;
  ExampleListData: ResolverTypeWrapper<GqlExampleListData>;
  NestedData: ResolverTypeWrapper<GqlNestedData>;
  Query: ResolverTypeWrapper<{}>;
  Subscription: ResolverTypeWrapper<{}>;
  User: ResolverTypeWrapper<GqlUser>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
};

/** Mapping between all available schema types and the resolvers parents */
export type GqlResolversParentTypes = {
  ExampleData: GqlExampleData;
  String: Scalars['String'];
  ExampleListData: GqlExampleListData;
  NestedData: GqlNestedData;
  Query: {};
  Subscription: {};
  User: GqlUser;
  Boolean: Scalars['Boolean'];
};

export type GqlExampleDataResolvers<
  ContextType = any,
  ParentType extends GqlResolversParentTypes['ExampleData'] = GqlResolversParentTypes['ExampleData'],
> = {
  someNumber?: Resolver<Maybe<GqlResolversTypes['String']>, ParentType, ContextType>;
  array?: Resolver<Maybe<Array<GqlResolversTypes['String']>>, ParentType, ContextType>;
  nestedData?: Resolver<Maybe<GqlResolversTypes['NestedData']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type GqlExampleListDataResolvers<
  ContextType = any,
  ParentType extends GqlResolversParentTypes['ExampleListData'] = GqlResolversParentTypes['ExampleListData'],
> = {
  liveId?: Resolver<GqlResolversTypes['String'], ParentType, ContextType>;
  exampleData?: Resolver<Maybe<GqlResolversTypes['ExampleData']>, ParentType, ContextType>;
  listData?: Resolver<Maybe<Array<Maybe<GqlResolversTypes['ExampleData']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type GqlNestedDataResolvers<
  ContextType = any,
  ParentType extends GqlResolversParentTypes['NestedData'] = GqlResolversParentTypes['NestedData'],
> = {
  someText?: Resolver<Maybe<GqlResolversTypes['String']>, ParentType, ContextType>;
  anotherArray?: Resolver<Maybe<Array<GqlResolversTypes['String']>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type GqlQueryResolvers<
  ContextType = any,
  ParentType extends GqlResolversParentTypes['Query'] = GqlResolversParentTypes['Query'],
> = {
  getAllUsers?: Resolver<Maybe<Array<Maybe<GqlResolversTypes['User']>>>, ParentType, ContextType>;
};

export type GqlSubscriptionResolvers<
  ContextType = any,
  ParentType extends GqlResolversParentTypes['Subscription'] = GqlResolversParentTypes['Subscription'],
> = {
  liveExampleListData?: SubscriptionResolver<
    Maybe<GqlResolversTypes['ExampleListData']>,
    'liveExampleListData',
    ParentType,
    ContextType,
    RequireFields<GqlSubscriptionLiveExampleListDataArgs, 'liveId'>
  >;
};

export type GqlUserResolvers<
  ContextType = any,
  ParentType extends GqlResolversParentTypes['User'] = GqlResolversParentTypes['User'],
> = {
  name?: Resolver<Maybe<GqlResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type GqlResolvers<ContextType = any> = {
  ExampleData?: GqlExampleDataResolvers<ContextType>;
  ExampleListData?: GqlExampleListDataResolvers<ContextType>;
  NestedData?: GqlNestedDataResolvers<ContextType>;
  Query?: GqlQueryResolvers<ContextType>;
  Subscription?: GqlSubscriptionResolvers<ContextType>;
  User?: GqlUserResolvers<ContextType>;
};
