import { ApolloError } from 'apollo-server-express';
import { PubSub } from 'graphql-subscriptions';
import { LiveManager } from '../.mono/server-node/live/liveManager';

const liveManager = new LiveManager(new PubSub(), {
  topics: ['liveExampleListData'],
});

let count = 0;
setInterval(async () => {
  count++;
  await liveManager.publish('liveExampleListData');
}, 1000);

export const ServiceResolvers = {
  Query: {
    getAllUsers: async (_: any, args: any) => {
      try {
        return [{ name: 'xyz' }, { name: 'abc' }];
      } catch (error: any) {
        throw new ApolloError(error);
      }
    },
  },
  Subscription: {
    liveExampleListData: {
      subscribe: async (parent: any, args: any, context: any, info: any): Promise<AsyncIterator<Partial<any>>> => {
        return liveManager.addSubscription('liveExampleListData', {
          preTrigger: () => console.log(`on pre`),
          postTrigger: () => console.log(`on post`),
        });
      },
    },
  },
  ExampleListData: {
    exampleData: () => ({ someNumber: 0 }),
    listData: () => [{ someNumber: 1 }, { someNumber: 2 }, { someNumber: 3 }, { someNumber: 4 }],
  },
  ExampleData: {
    array: () => {
      return ['an', 'array', 'with', 'content'];
    },
    nestedArray: (parent: any) => {
      const magicVal = Math.floor((count + parent.someNumber) / 5);
      return magicVal % 5 > 2 ? [parent] : [parent, parent];
    },
  },
  NestedData: {
    someText: (parent: any) => {
      return `some text content for ${parent.someNumber}`;
    },
    anotherArray: (parent: any) => {
      const magicVal = Math.floor((count + parent.someNumber) / 5);
      return [
        { keyA: '111', keyB: 'static B first' },
        ...(parent.someNumber % 5 > 3
          ? [
              { keyA: '222', keyB: 'second B' },
              { keyA: '333', keyB: 'third B' },
            ]
          : [{ keyA: '222', keyB: 'second no third B' }]),
        { keyA: '444', keyB: `${magicVal}` },
        { keyA: '555', keyB: `lastly` },
      ];
    },
  },
};
