import { _liveKeyword, _liveState, liveRequestHandlerBuilder } from './liveRequestHandler';
import { InMemLiveState } from '../../.mono/model/live-state';
import { FetchResult, RequestHandler } from '@apollo/client/link/core/types';
import { Observable } from '@apollo/client';
import { patch } from 'jsondiffpatch';
import { applyPatch } from 'fast-json-patch';

jest.mock('jsondiffpatch', () => ({
  patch: jest.fn().mockReturnValue({ jsondiffpatch: 'patch' }),
}));

jest.mock('fast-json-patch', () => ({
  applyPatch: jest.fn().mockReturnValue({ newDocument: { 'fast-json-patch': 'applyPatch' } }),
}));

const captureObservable = (mock: jest.Mock) => {
  return { onNext: mock.mock.calls[0][0], onError: mock.mock.calls[0][1], onComplete: mock.mock.calls[0][2] };
};

describe('liveRequestHandler', () => {
  describe('liveRequestHandlerBuilder', () => {
    it('builds and sets global values', async () => {
      const subscribe = liveRequestHandlerBuilder('TEST_STATE' as any, 'TEST_KEY');
      expect(subscribe).toBeDefined();
      expect(_liveState).toEqual('TEST_STATE');
      expect(_liveKeyword).toEqual('TEST_KEY');
    });
  });

  describe('liveRequestHandler', () => {
    let inMemLiveState: InMemLiveState;
    let liveKeyword: string;
    let liveRequestHandler: RequestHandler;

    let forwardMock: jest.Mock;
    let subscriptionMock: jest.Mock;
    let unsubscribeMock: jest.Mock;

    let handler: Observable<FetchResult>;

    beforeEach(() => {
      inMemLiveState = new InMemLiveState();
      liveKeyword = 'LIVE_KEY';
      liveRequestHandler = liveRequestHandlerBuilder(inMemLiveState, liveKeyword);

      forwardMock = jest.fn();
      subscriptionMock = jest.fn();
      unsubscribeMock = jest.fn();

      forwardMock.mockReturnValue({
        subscribe: subscriptionMock,
      });
      subscriptionMock.mockReturnValue({
        unsubscribe: unsubscribeMock,
      });

      handler = liveRequestHandler('TEST_OPERATION' as any, forwardMock)!!;
    });

    it('wraps and subscribes to forward operation', async () => {
      const subscribe = handler.subscribe({});

      expect(forwardMock).toHaveBeenCalledWith('TEST_OPERATION');
      expect(subscriptionMock).toHaveBeenCalled();
      expect(unsubscribeMock).toHaveBeenCalledTimes(0);

      subscribe.unsubscribe();
      expect(unsubscribeMock).toHaveBeenCalled();
    });

    it('forwards errors', async () => {
      const errorMock = jest.fn();
      handler.subscribe(jest.fn(), errorMock, jest.fn());

      const { onError } = captureObservable(subscriptionMock);
      onError('TEST_ERROR');

      expect(errorMock).toHaveBeenCalledWith('TEST_ERROR');
    });

    it('forwards complete', async () => {
      const completeMock = jest.fn();
      handler.subscribe(jest.fn(), jest.fn(), completeMock);

      const { onComplete } = captureObservable(subscriptionMock);
      onComplete();

      expect(completeMock).toHaveBeenCalled();
    });

    it('forwards non live data', async () => {
      const nextMock = jest.fn();
      handler.subscribe(nextMock, jest.fn());

      const { onNext } = captureObservable(subscriptionMock);
      const testData = { data: { key: 'VALUE' } };
      onNext(testData);

      expect(nextMock).toHaveBeenCalledWith(testData);
    });

    describe('on live data', () => {
      let liveDataA: Record<string, any>;
      let liveDataB: Record<string, any>;
      let testData: { data: Record<string, Record<string, any>> };

      beforeEach(() => {
        liveDataA = { liveId: 'TEST_LIVE_ID_A', key: 'VALUE_A' };
        liveDataB = { liveId: 'TEST_LIVE_ID_B', key: 'VALUE_B' };
        testData = {
          data: {
            [liveKeyword + '_A']: liveDataA,
            [liveKeyword + '_B']: liveDataB,
            someOtherKey: { key: 111 },
          },
        };
      });

      it('adds zero count to fresh live id and calls next', async () => {
        const nextMock = jest.fn();
        handler.subscribe(nextMock, jest.fn());
        const { onNext } = captureObservable(subscriptionMock);

        expect(inMemLiveState.get('TEST_LIVE_ID')).toEqual(undefined);

        onNext(JSON.parse(JSON.stringify(testData)));

        liveDataA.liveId = '0-TEST_LIVE_ID_A';
        liveDataB.liveId = '0-TEST_LIVE_ID_B';
        expect(nextMock).toHaveBeenCalledWith(testData);
      });

      it('store new liveIds in the cache', async () => {
        const nextMock = jest.fn();
        handler.subscribe(nextMock, jest.fn());
        const { onNext } = captureObservable(subscriptionMock);

        expect(inMemLiveState.get('TEST_LIVE_ID')).toEqual(undefined);

        onNext(JSON.parse(JSON.stringify(testData)));

        expect(inMemLiveState.get(liveDataA.liveId)).toEqual(liveDataA);
        expect(inMemLiveState.get(liveDataB.liveId)).toEqual(liveDataB);
      });

      describe('clears cache for liveIds', () => {
        let subscription: any;
        let captured: any;

        beforeEach(() => {
          const nextMock = jest.fn();
          subscription = handler.subscribe(nextMock, jest.fn());
          captured = captureObservable(subscriptionMock);

          inMemLiveState.set('TEST_LIVE_ID_C', 'TEST_VALUE_C');
          captured.onNext(JSON.parse(JSON.stringify(testData)));
        });

        it('on unsubscribe', async () => {
          subscription.unsubscribe();

          expect(inMemLiveState.get(liveDataA.liveId)).toEqual(undefined);
          expect(inMemLiveState.get(liveDataB.liveId)).toEqual(undefined);
          expect(inMemLiveState.get('TEST_LIVE_ID_C')).toEqual('TEST_VALUE_C');
        });

        it('on error', async () => {
          captured.onError();

          expect(inMemLiveState.get(liveDataA.liveId)).toEqual(undefined);
          expect(inMemLiveState.get(liveDataB.liveId)).toEqual(undefined);
          expect(inMemLiveState.get('TEST_LIVE_ID_C')).toEqual('TEST_VALUE_C');
        });

        it('on complete', async () => {
          captured.onComplete();

          expect(inMemLiveState.get(liveDataA.liveId)).toEqual(undefined);
          expect(inMemLiveState.get(liveDataB.liveId)).toEqual(undefined);
          expect(inMemLiveState.get('TEST_LIVE_ID_C')).toEqual('TEST_VALUE_C');
        });
      });

      describe('for patch/diff updates', () => {
        let nextMock: jest.Mock;
        let onNext: any;

        beforeEach(() => {
          nextMock = jest.fn();
          handler.subscribe(nextMock, jest.fn());
          const captured = captureObservable(subscriptionMock);
          onNext = captured.onNext;
          onNext(JSON.parse(JSON.stringify(testData)));
        });

        it('applies the patch and returns result', async () => {
          const patchContent = { partial: 'TEST_PATCH' };
          const testDataCopy = JSON.parse(JSON.stringify(testData));
          testDataCopy.data[liveKeyword + '_A'] = {
            ...liveDataA,
            patch: JSON.stringify(patchContent),
            count: 1,
          };
          const expected = JSON.parse(JSON.stringify(testData));

          onNext(testDataCopy);

          expected.data[liveKeyword + '_A'] = {
            'fast-json-patch': 'applyPatch',
            count: 1,
            liveId: `1-${liveDataA.liveId}`,
          };
          expected.data[liveKeyword + '_B'] = { ...liveDataB, liveId: `0-${liveDataB.liveId}` };
          expect(applyPatch).toHaveBeenCalledWith(liveDataA, patchContent, undefined, false);
          expect(nextMock).toHaveBeenNthCalledWith(2, expected);
        });

        it('applies the diff and returns result', async () => {
          const diffContent = { partial: 'TEST_DIFF' };
          const testDataCopy = JSON.parse(JSON.stringify(testData));
          testDataCopy.data[liveKeyword + '_A'] = { ...liveDataA, diff: JSON.stringify(diffContent), count: 1 };
          const expected = JSON.parse(JSON.stringify(testData));

          onNext(testDataCopy);

          expected.data[liveKeyword + '_A'] = {
            jsondiffpatch: 'patch',
            count: 1,
            liveId: `1-${liveDataA.liveId}`,
          };
          expected.data[liveKeyword + '_B'] = { ...liveDataB, liveId: `0-${liveDataB.liveId}` };
          expect(patch).toHaveBeenCalledWith(liveDataA, diffContent);
          expect(nextMock).toHaveBeenNthCalledWith(2, expected);
        });

        describe('errors', () => {
          it('when no local cache exist for patch', async () => {
            const patchContent = { partial: 'TEST_PATCH' };
            const testDataCopy = JSON.parse(JSON.stringify(testData));
            testDataCopy.data[liveKeyword + '_D'] = {
              ...liveDataA,
              liveId: 'SOME ID',
              patch: JSON.stringify(patchContent),
              count: 1,
            };

            expect(() => onNext(testDataCopy)).toThrow();
          });

          it('when local count doesnt match new count', async () => {
            const patchContent = { partial: 'TEST_PATCH' };
            const testDataCopy = JSON.parse(JSON.stringify(testData));
            testDataCopy.data[liveKeyword + '_A'] = {
              ...liveDataA,
              patch: JSON.stringify(patchContent),
              count: 10,
            };

            await expect(() => onNext(testDataCopy)).toThrow();
          });
        });
      });
    });
  });
});
